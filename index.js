//making server
import express from 'express';
import fs from 'fs';
import zlib from 'zlib' //for zipping the file

const app = express();

const PORT = 8000;


//suppose we wanr to zip our text file as ourt file is 400mb thne the4 problem will be the server will read the file then zip the file and then send it
//400mb (read) --> 400mb(Zip) --> 400mb(write/send) //same problem 
//we can do ut by streeams acan read the file in chunks and zip the file with zlib lib


//stream Read (file.txt) --> send it to Zipper --> output stream krdia fs write Stream 
//zipping the file
//file reead hogi then file zip hogi adn then file write ho jayegi .zip withouit using our memory.
fs.createReadStream('file.txt').pipe(zlib.createGzip().pipe(fs.createWriteStream('file.zip')));

app.get("/", (req, res) => {
    // return res.json({ message: "Server is healthy." })
    //this below code without streams in node js
    // fs.readFile('file.txt', (err, data) => {
    //     res.end(data);
    // });

    //improving efficiency of our code by using concept os streams
    const stream = fs.createReadStream("file.txt", "utf-8");
    //jb bhi data ayega tb hamre pass uss data ka chunk ayega 
    stream.on('data', (chunk) => {
        res.write(chunk);
    });

    //jb bhi strsm end hogi tb hm request and response cycle ko end kr denge. by res.end.
    stream.on('end', () => {
        res.end();
    });
});

app.listen(PORT, () => {
    console.log(`Server is running at port ${PORT}`);
});